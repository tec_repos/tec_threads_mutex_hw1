/*
Run command:
gcc ArrayAway.c -o array.out && ./array.out

Luis Diego Mora Aguilar.
2018147110.
Tarea 2 - Principios de Sistemas Operativos.

Instrucciones:
    Escriba un programa que cuente con 10 arreglos con hasta 100 números enteros cada uno, 
    inicialmente todos contendrán 50 números aleatorios y el resto serán valores en 0 (nulos). 
    Se creará un hilo por cada arreglo de forma que se encargue de mantener solo números 
    cuyo último dígito coincida con el número de arreglo, por ejemplo, el arreglo 0 podrán 
    contener el 10,100,140,1200,etc. y el arreglo 9 podrá contener números 
    como 99,2349,19,239,etc. Si un hilo encuentra en su arreglo un número que no coincide 
    debe "moverlo" a una posición con valor nulo en el arreglo que le corresponde y eliminarlo 
    de su arreglo actual (ponerlo en nulo). Si no existen espacios nulos en el arreglo al 
    que se va a mover alguno de los valores, entonces ese valor simplemente se desecha. 
    Tome en cuenta que al mover los números se deben bloquear los dos arreglos involucrados 
    en la operación.

*/

#include <pthread.h>
#include <stdio.h>

pthread_mutex_t mutexArray[10];
int numberArrays[10][100];


int getRandom(int pLimit){
  return rand() % pLimit;
}


void* checkArray(void *arg) {
  int status;
	int position = (int)arg;
  for (int number = 0; number < 100; number++)
	{
		if(number < 50){
			int lastNum = numberArrays[position][number] % 10;
			if(lastNum != position){
				status = pthread_mutex_lock(&mutexArray[position]);
				status = pthread_mutex_lock(&mutexArray[lastNum]);
				for (int anotherNum = 50; anotherNum < 100; anotherNum++){
					if(numberArrays[lastNum][anotherNum] != 0){
						numberArrays[lastNum][anotherNum] = numberArrays[position][number];
					}
				}
				numberArrays[position][number] = 0;
				status = pthread_mutex_unlock(&mutexArray[lastNum]);
				status = pthread_mutex_unlock(&mutexArray[position]);
			}
		}
	}
  return NULL;
}


int main (int argh, char *argv[]) {
	srand(time(NULL));
  int status;
	for (int indexMutex = 0; indexMutex < 10; indexMutex++)
		status = pthread_mutex_init(&mutexArray[indexMutex],NULL);
  pthread_t thread[10];
  
	for (int array = 0; array < 10; array++){
		for (int number = 0; number < 100; number++){
			if(number < 50){
				numberArrays[array][number] = getRandom(10000);
			}
			else{
				numberArrays[array][number] = 0;
			}
		}
	}
  for (int indexThread=0; indexThread<10; indexThread++) {
    status = pthread_create(&thread[indexThread],NULL, checkArray,(void *)indexThread);
  }
  for (int i=0; i<10; i++)
    status = pthread_join(thread[i],NULL);
	for (int array = 0; array < 10; array++){
		printf("\n");
		for (int number = 0; number < 100; number++){
			printf("%i ", numberArrays[array][number]);
		}
	}
	for (int indexMutex = 0; indexMutex < 10; indexMutex++)
		status = pthread_mutex_destroy(&mutexArray[indexMutex]);
}