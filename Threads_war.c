/*
Run command:
gcc Threads_war.c -o war.out && ./war.out

Luis Diego Mora Aguilar.
2018147110.
Tarea 2 - Principios de Sistemas Operativos.

Instrucciones:
    Guerra de hilos: Escriba un programa que cree 10 hilos que tratarán de matarse entre sí. 
    Antes de matar otro hilo se debe publicar en un arreglo compartido el ID del hilo que se 
    va a liquidar, dormir una cantidad aleatoria de segundos (0-10 segundos), y luego matarlo. 
    Los hilos se pueden defender tratando de matar antes al hilo que los va a atacar 
    (pero también deben dormir una cantidad aleatoria de segundos antes de hacerlo).

*/

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdlib.h>


//Ejercicio 1: Guerra de Hilos
#define NUM_THREADS 10
pthread_t thread_soldiers[NUM_THREADS];
pthread_t aimedTargets[NUM_THREADS];
int startWarFlag = 0;   //0 for false and 1 for true


int getRandom(int pLimit){
  return rand() % pLimit;
}


void checkRadar(int pPosition){
  if(aimedTargets[pPosition] != 0){
    printf("\tIncoming attack, preparing counter attack against %ld...\n", (long)aimedTargets[pPosition]);
    sleep(getRandom(10));
    pthread_cancel(aimedTargets[pPosition]);
    printf("\tSoldier: %ld killed %ld by counter attack\n", (long)pthread_self(), 
            (long)aimedTargets[pPosition]);
    aimedTargets[pPosition] = 0;
  }
}


void* startWar(void *soldierResources){
  while(startWarFlag == 0){
    sleep(1);
  }
  long soldierPosition = (long)soldierResources;
  checkRadar(soldierPosition);
  int breathBeforeAtk = getRandom(10);
  sleep(breathBeforeAtk);
  pthread_t aimTarget = pthread_self();
  int indexThread = 0;
  checkRadar(soldierPosition);
  while(aimTarget == pthread_self()){
    indexThread = getRandom(NUM_THREADS);
    aimTarget = thread_soldiers[indexThread];
  }
  checkRadar(soldierPosition);
  printf("Aim Target: %ld\n", (long)aimTarget);
  aimedTargets[indexThread] = pthread_self();
  sleep(getRandom(10));
  pthread_cancel(aimTarget);
  checkRadar(soldierPosition);
  printf("Soldier: %ld killed %ld\n", (long)pthread_self(), (long)aimTarget);
}


int main(int argh, char *argv[]) {
  srand(time(NULL));

  int returnCode;
  long indexThread;
  for (indexThread = 0; indexThread < NUM_THREADS; indexThread++) {
    aimedTargets[indexThread] = 0;
    returnCode = pthread_create(&thread_soldiers[indexThread], NULL, startWar,
                        (void *)indexThread);
    if (returnCode) {
      printf("ERROR; return code from pthread_create() is %d\n", returnCode);
      exit(-1);
    }
  }
  for (indexThread = 0; indexThread < NUM_THREADS; indexThread++) {
    printf("Soldier ID created is: %ld\n", (long)thread_soldiers[indexThread]);
  }
  startWarFlag = 1;
  /* Last thing that main() should do */
  pthread_exit(NULL);
}