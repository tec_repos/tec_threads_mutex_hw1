# TEC_Threads_Mutex_HW1

Instrucciones:

1- Guerra de hilos: Escriba un programa que cree 10 hilos que tratarán de matarse entre sí. Antes de matar otro hilo se debe publicar en un arreglo compartido el ID del hilo que se va a liquidar, dormir una cantidad aleatoria de segundos (0-10 segundos), y luego matarlo. Los hilos se pueden defender tratando de matar antes al hilo que los va a atacar (pero también deben dormir una cantidad aleatoria de segundos antes de hacerlo).

2- Escriba un programa que cuente con 10 arreglos con hasta 100 números enteros cada uno, inicialmente todos contendrán 50 números aleatorios y el resto serán valores en 0 (nulos). Se creará un hilo por cada arreglo de forma que se encargue de mantener solo números cuyo último dígito coincida con el número de arreglo, por ejemplo, el arreglo 0 podrán contener el 10,100,140,1200,etc. y el arreglo 9 podrá contener números como 99,2349,19,239,etc. Si un hilo encuentra en su arreglo un número que no coincide debe "moverlo" a una posición con valor nulo en el arreglo que le corresponde y eliminarlo de su arreglo actual (ponerlo en nulo). Si no existen espacios nulos en el arreglo al que se va a mover alguno de los valores, entonces ese valor simplemente se desecha. Tome en cuenta que al mover los números se deben bloquear los dos arreglos involucrados en la operación.

## Name
TEC_Threads_Mutex_Homework1

## Description
This homework is intended to learn about threads and mutex shared structures, so we have 2 exercises, each one has a .c file with intern documentation.

## Installation
Just Unix environment with gcc.

## Authors and acknowledgment
[Luis Diego Mora Aguilar](https://www.linkedin.com/in/luis-diego-mora-aguilar-741741145/)

## Project status
Done and delivered.
